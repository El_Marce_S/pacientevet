import React, {Component} from 'react';
import './bootstrap.min.css'
import Header from './Components/Header'
import NuevaCita from './Components/NuevaCita'
import ListaCitas from './Components/ListaCitas'
// import Cita from "./Components/Cita";


class App extends Component {
    state = {
        Citas: []
    }

    //Cuando la Aplicacion Carga
    componentDidMount() {
        const citasLS = localStorage.getItem('Citas');
        if (citasLS){
            this.setState({
                Citas: JSON.parse(citasLS)
            })
        }
    }


    //Cuando Eliminamos o agergamos una cita
    componentDidUpdate() {
        localStorage.setItem('Citas',JSON.stringify(this.state.Citas));
    }


    CrearNuevaCita = datos => {
        //Copiar el State Actual
        const Citas = [...this.state.Citas, datos]

        //agregar el nuevo State
        this.setState({
            Citas: Citas
        })
    }

    //Elimina las citas dekl State
    eliminarCita = id => {

        //RealizarCopia del State
        const citasActuales = [...this.state.Citas];

        //Utilizar Filter para sacar el elmento ID @id del arreglo
        const Citas = citasActuales.filter(cita => cita.id !== id);

        //Actualizar el State
        this.setState({Citas})
    }

    render() {
        return (
            <div className="container">
                <Header
                    titulo='Administrador de paciente Veterinaria'>
                </Header>
                <div className="row">
                    <div className="col-md-10 mx-auto">
                        <NuevaCita
                            crearNuevaCita={this.CrearNuevaCita}></NuevaCita>
                    </div>
                    <div className='mt-5 col-md-10 mx-auto'>
                        <ListaCitas
                            citas={this.state.Citas}
                            eliminarCita={this.eliminarCita}

                        ></ListaCitas>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
