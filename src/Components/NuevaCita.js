import React, {Component, Fragment} from 'react';
import uuid from 'uuid';
import PropTypes from 'prop-types';

const StateInicial = {
    Cita: {
        mascota: '',
        Propietario: '',
        Fecha: '',
        Hora: '',
        Sintomas: ''
    },
    error: false

}

class NuevaCita extends Component {
    state = {
        ...StateInicial
    };
// cuando el usuario escribe en los campos del formulario
    handleChange = e => {
        // console.log(e.target.name + ': ' + e.target.value );
        //colocar el valor de lo ingresado
        this.setState({
            Cita: {
                ...this.state.Cita,
                [e.target.name]: e.target.value
            }
        })
    }
    //Cuando el Usuario hace click en el envio del formulario
    handleSubmit = e => {
        e.preventDefault();
        //Extramos Valores del State
        const {mascota, Propietario, Fecha, Hora, Sintomas} = this.state.Cita;

        //Validamos
        if (mascota === '' || Propietario === '' || Fecha === '' || Hora === '' || Sintomas === '') {
            this.setState({
                error: true
            });
            //Detener la ejecucion
            return;

        }

        //Generar Objeto Con Datos
        const nuevaCita = {...this.state.Cita};
        nuevaCita.id = uuid();

        //Agregar la cita al State de APP
        this.props.crearNuevaCita(nuevaCita)
// Colocar en el State el Satet= Inicial
        this.setState({
            ...StateInicial
        })
    }

    render() {
        //Extraer Valor del State
        const {error} = this.state;

        return (
            <div className="card mt-5 py-5">
                <div className="card-body">
                    <h2 className="card-title text-center mb-5">
                        Llena el Formulario para crear una nueva cita.
                    </h2>

                    {error ? <div className="alert alert-danger mt-2 mb-5 text-center">Todos los Campos Son
                        Obligatorios</div> : null}
                    <form onSubmit={this.handleSubmit}>
                        <div className="form-group row">
                            <label className='col-sm-4 col-lg-2 col-form-label'>
                                Nombre de la Mascota:
                            </label>
                            <div className='col-sm-8 col-lg-10'>
                                <input
                                    type="text"
                                    className='form-control'
                                    placeholder="Nombre de Mascota"
                                    name='mascota'
                                    onChange={this.handleChange}
                                    value={this.state.Cita.mascota}
                                />
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className='col-sm-4 col-lg-2 col-form-label'>
                                Nombre del dueño de la mascota:
                            </label>
                            <div className='col-sm-8 col-lg-10'>
                                <input
                                    type="text"
                                    className='form-control'
                                    placeholder="Dueño de la mascota"
                                    name='Propietario'
                                    onChange={this.handleChange}
                                    value={this.state.Cita.Propietario}
                                />
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className='col-sm-4 col-lg-2 col-form-label'>
                                Fecha de registro:
                            </label>
                            <div className='col-sm-8 col-lg-4'>
                                <input
                                    type="date"
                                    className='form-control'
                                    name='Fecha'
                                    onChange={this.handleChange}
                                    value={this.state.Cita.Fecha}
                                />
                            </div>

                            <label className='col-sm-4 col-lg-2 col-form-label'>
                                Hora de registro:
                            </label>
                            <div className='col-sm-8 col-lg-4'>
                                <input
                                    type="time"
                                    className='form-control'
                                    name='Hora'
                                    onChange={this.handleChange}
                                    value={this.state.Cita.Hora}
                                />
                            </div>
                        </div>
                        <div className="form-group row">
                            <label className='col-sm-4 col-lg-2 col-form-label'>
                                Sintomas:
                            </label>
                            <div className='col-sm-8 col-lg-10'>
                                <textarea cols="30" rows="10"
                                          className="form-control"
                                          name="Sintomas"
                                          placeholder='Describe los sintomas'
                                          onChange={this.handleChange}
                                          value={this.state.Cita.Sintomas}
                                >
                                </textarea>
                            </div>
                            <input type="submit" className='py-3 mt-2 btn btn-success btn-block '
                                   value='Agregar nueva cita'/>
                        </div>
                    </form>
                </div>
            </div>
        );
    };

}

NuevaCita.propTypes ={
    crearNuevaCita:PropTypes.func.isRequired
}

export default NuevaCita;
